from sklearn.externals import joblib


class Model:

    def __init__(self, model_path='models/'):
        self.classifier = joblib.load(model_path +'classifier.pkl')
        self.vectorizer = joblib.load(model_path + 'vectorizer.pkl')

    def classify(self, message):
        transform_text = self.vectorizer.transform([message])
        prediction = self.classifier.predict(transform_text)[0]
        return int(prediction)


def make_model(model_path='models/'):
    return Model(model_path)