from . import app
from .model import make_model
from flask import jsonify, request


model = make_model('./flask_app/models/')

@app.route('/classify', methods=['GET', 'POST'])
def index():
    content = request.json
    if 'text' in content:
        text = content['text']
        prediction = model.classify(text)
        response = jsonify({'class': prediction})
    else:
        response = jsonify({"class": None})
        response.status_code = 500

    return response
